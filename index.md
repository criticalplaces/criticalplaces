---
title: Home
banner_image: "/uploads/2018/09/27/crticalhands.jpg"
layout: landing-page
heading: Critical Places
partners:
- "/uploads/2017/11/13/stem.png"
- "/uploads/2017/11/13/UPenn_logo.png"
- "/uploads/2017/11/13/nysed.png"
services:
- description: Performing collaborative research and providing services to support
    the Health Sector.
  heading: Health
  icon: "/uploads/2017/11/13/health.png"
- description: Performing collaborative research and providing services to support
    the biotechnology sector.
  heading: BioTech
  icon: "/uploads/2017/11/13/biotech.png"
sub_heading: " Transformation through Architecture!!!"
textline: Critical Places
hero_button:
  text: Learn more
  href: "/about"
show_news: true
show_staff: false
menu:
  navigation:
    identifier: _index
    weight: 1

---
